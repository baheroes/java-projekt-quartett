package application;

import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public final class Animation {

	private Animation() {
	}

	// Vorderseite Computer Karte anzeigen
	public static void flipBackToFront(VBox cCard, ImageView back, final EventHandler<ActionEvent> onFinished) {
		// Rueckseite minimieren
		ScaleTransition stHideFront = new ScaleTransition(Duration.millis(500), back);
		stHideFront.setFromX(1);
		stHideFront.setToX(0);
		cCard.setScaleX(0);
		// Vorderseite maximieren
		ScaleTransition stShowBack = new ScaleTransition(Duration.millis(500), cCard);
		stShowBack.setFromX(0);
		stShowBack.setToX(1);
		// Event starten
		stHideFront.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				stShowBack.play();
				if (onFinished != null)
					onFinished.handle(t);
			}
		});
		stHideFront.play();
	}

	// Rueckseite Computer Karte anzeigen
	public static void flipFrontToBack(VBox cCard, ImageView back, EventHandler<ActionEvent> onFinished) {
		// Vorderseite minimieren
		ScaleTransition stHideFront = new ScaleTransition(Duration.millis(1), cCard);
		stHideFront.setFromX(1);
		stHideFront.setToX(0);
		back.setScaleX(0);
		// Rueckseite maximieren
		ScaleTransition stShowBack = new ScaleTransition(Duration.millis(1), back);
		stShowBack.setFromX(0);
		stShowBack.setToX(1);
		// Event starten
		stHideFront.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				stShowBack.play();
				if (onFinished != null)
					onFinished.handle(t);
			}
		});
		stHideFront.play();
	}
}
