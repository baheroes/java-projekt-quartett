package application;

import java.util.ArrayList;
import java.util.Arrays;

public class RankData {
	private Integer ID;
	private Integer val1Rank;
	private Integer val2Rank;
	private Integer val3Rank;
	private Integer val4Rank;
	private Integer val5Rank;
	private Integer val6Rank;

	// ID
	public Integer getID() {
		return ID;
	}

	public void setID(Integer ID) {
		this.ID = ID;
	}

	// Rang Wert 1
	public Integer getVal1Rank() {
		return val1Rank;
	}

	public void setVal1Rank(Integer val1Rank) {
		this.val1Rank = val1Rank;
	}

	// Rang Wert 2
	public Integer getVal2Rank() {
		return val2Rank;
	}

	public void setVal2Rank(Integer val2Rank) {
		this.val2Rank = val2Rank;
	}

	// Rang Wert 3
	public Integer getVal3Rank() {
		return val3Rank;
	}

	public void setVal3Rank(Integer val3Rank) {
		this.val3Rank = val3Rank;
	}

	// Rang Wert 4
	public Integer getVal4Rank() {
		return val4Rank;
	}

	public void setVal4Rank(Integer val4Rank) {
		this.val4Rank = val4Rank;
	}

	// Rang Wert 5
	public Integer getVal5Rank() {
		return val5Rank;
	}

	public void setVal5Rank(Integer val5Rank) {
		this.val5Rank = val5Rank;
	}

	// Rang Wert 6
	public Integer getVal6Rank() {
		return val6Rank;
	}

	public void setVal6Rank(Integer val6Rank) {
		this.val6Rank = val6Rank;
	}

	// Rang Liste befuellen mit Informationen aus dem Kartenstapel
	public ArrayList<RankData> createRankList() {

		// Rangliste erstellen
		ArrayList<RankData> rankList = new ArrayList<RankData>();

		// Fahrzeugliste erstellen
		ArrayList<Car> helpStack = DeckReader.readDeck();

		// Hilfsvariablen f�r Sortierung festlegen
		int a = helpStack.size();

		// Rangliste mit Objekten befuellen und ID zuweisen
		for (int i = 0; i < a; i++) {
			RankData Position = new RankData();
			Position.setID(i + 1);
			rankList.add(Position);
		}

		// Value 1 (Preis)
		Double v1[] = new Double[a];
		for (int i = 0; i < a; i++) {
			double singleV1 = helpStack.get(i).getPrice();
			v1[i] = singleV1;
		}
		Arrays.sort(v1);

		// Suche nach Position des Preises aus dem Auto-Stapel und setze
		// entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			double singleV1 = helpStack.get(i).getPrice();
			int b = Arrays.binarySearch(v1, singleV1);
			if (helpStack.get(1).getV1LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal1Rank(invB);
			} else {
				rankList.get(i).setVal1Rank(b);
			}
		}

		// Value 2 (Hubraum)
		Integer v2[] = new Integer[a];
		for (int i = 0; i < a; i++) {
			int singleV2 = helpStack.get(i).getCapa();
			v2[i] = singleV2;
		}
		Arrays.sort(v2);

		// Suche nach Position des Hubraums aus dem Auto-Stapel und setze
		// entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			int singleV2 = helpStack.get(i).getCapa();
			int b = Arrays.binarySearch(v2, singleV2);
			if (helpStack.get(1).getV2LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal2Rank(invB);
			} else {
				rankList.get(i).setVal2Rank(b);
			}
		}

		// Value 3 (Leistung)
		Integer v3[] = new Integer[a];
		for (int i = 0; i < a; i++) {
			int singleV3 = helpStack.get(i).getPower();
			v3[i] = singleV3;
		}
		Arrays.sort(v3);

		// Suche nach Position der Leistung aus dem Auto-Stapel und setze
		// entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			int singleV3 = helpStack.get(i).getPower();
			int b = Arrays.binarySearch(v3, singleV3);
			if (helpStack.get(1).getV3LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal3Rank(invB);
			} else {
				rankList.get(i).setVal3Rank(b);
			}
		}

		// Value 4 (Beschleunigung)
		Double v4[] = new Double[a];
		for (int i = 0; i < a; i++) {
			double singleV4 = helpStack.get(i).getAcc();
			v4[i] = singleV4;
		}
		Arrays.sort(v4);

		// Suche nach Position der Beschleunigung aus dem Auto-Stapel und setze
		// entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			double singleV4 = helpStack.get(i).getAcc();
			int b = Arrays.binarySearch(v4, singleV4);
			if (helpStack.get(1).getV4LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal4Rank(invB);
			} else {
				rankList.get(i).setVal4Rank(b);
			}
		}

		// Value 5 (Hoechstgeschwindigkeit)
		Integer v5[] = new Integer[a];
		for (int i = 0; i < a; i++) {
			int singleV5 = helpStack.get(i).getVmax();
			v5[i] = singleV5;
		}
		Arrays.sort(v5);

		// Suche nach Position der Hoechstgeschwindigkeit aus dem Auto-Stapel
		// und setze entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			int singleV5 = helpStack.get(i).getVmax();
			int b = Arrays.binarySearch(v5, singleV5);
			if (helpStack.get(1).getV5LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal5Rank(invB);
			} else {
				rankList.get(i).setVal5Rank(b);
			}
		}

		// Value 6 (Verbrauch)
		Double v6[] = new Double[a];
		for (int i = 0; i < a; i++) {
			double singleV6 = helpStack.get(i).getCons();
			v6[i] = singleV6;
		}
		Arrays.sort(v6);

		// Suche nach Position des Verbrauchs aus dem Auto-Stapel und setze
		// entsprechend den Rang fuer den Wert in der Rangliste
		for (int i = 0; i < a; i++) {
			double singleV6 = helpStack.get(i).getCons();
			int b = Arrays.binarySearch(v6, singleV6);
			if (helpStack.get(1).getV6LowToWin() == true) {
				int invB = helpStack.size() - b;
				rankList.get(i).setVal6Rank(invB);
			} else {
				rankList.get(i).setVal6Rank(b);
			}

		}
		return rankList;
	}
}
