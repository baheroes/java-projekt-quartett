package application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DeckReader {

	private final static String csvFileToRead = "/csvFolder/Autoquartett.csv";

	// Eine CSV-Datei einlesen.
	public static ArrayList<Car> readDeck() {
		BufferedReader br = null;
		String line = "";
		String splitBy = ";";
		ArrayList<Car> cardStack = new ArrayList<Car>();

		try {
			URL url = DeckReader.class.getResource(csvFileToRead);
			br = new BufferedReader(new InputStreamReader(url.openStream()));

			// Erzeuge ein Objekt fuer jede Zeile der CSV-Datei.
			while ((line = br.readLine()) != null) {

				// Den Trenner definieren (in diesem Fall ";").
				String[] cars = line.split(splitBy);

				// Ein Objekt erzeugen.
				Car carObject = new Car();

				// Die Werte aus der CSV-Datei dem Objekt zuweisen.
				carObject.setID(Integer.parseInt(cars[0]));
				carObject.setName(cars[1]);
				carObject.setPrice(Double.parseDouble(cars[2]));
				carObject.setCapa(Integer.parseInt(cars[3]));
				carObject.setPower(Integer.parseInt(cars[4]));
				carObject.setAcc(Double.parseDouble(cars[5]));
				carObject.setVmax(Integer.parseInt(cars[6]));
				carObject.setCons(Double.parseDouble(cars[7]));
				carObject.setV1Unit(cars[8]);
				carObject.setV2Unit(cars[9]);
				carObject.setV3Unit(cars[10]);
				carObject.setV4Unit(cars[11]);
				carObject.setV5Unit(cars[12]);
				carObject.setV6Unit(cars[13]);
				carObject.setV1LowToWin(Boolean.parseBoolean(cars[14]));
				carObject.setV2LowToWin(Boolean.parseBoolean(cars[15]));
				carObject.setV3LowToWin(Boolean.parseBoolean(cars[16]));
				carObject.setV4LowToWin(Boolean.parseBoolean(cars[17]));
				carObject.setV5LowToWin(Boolean.parseBoolean(cars[18]));
				carObject.setV6LowToWin(Boolean.parseBoolean(cars[19]));

				// Objekt "Car" der Liste zuweisen.
				cardStack.add(carObject);
			}
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cardStack;

	}

	// Methode um eine vollstaendige Liste auszudrucken.
	public void printDeck(List<Car> carListToPrint) {
		for (int i = 0; i < carListToPrint.size(); i++) {
			System.out.println("CARS [" + "ID=" + (carListToPrint.get(i)).getID() + " , Name= "
					+ (carListToPrint.get(i)).getName() + " , Preis=" + (carListToPrint.get(i)).getPrice()
					+ " , Hubraum=" + (carListToPrint.get(i)).getCapa() + " , Leistung="
					+ (carListToPrint.get(i)).getPower() + " , Beschleunigung=" + (carListToPrint.get(i)).getAcc()
					+ " , Geschwindigkeit=" + (carListToPrint.get(i)).getVmax() + " , Verbrauch="
					+ (carListToPrint.get(i)).getCons() + "]");
		}
	}

}
