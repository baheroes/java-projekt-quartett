package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class Main extends Application {

	private static Stage primaryStage;
	public static String inputtext;
	public static TextField name;
	
	// Main Methode
	public static void main(String[] args) {
		launch(args);
	}
	// Stage Methode
	protected static Stage getStage() {
		return primaryStage;
	}

	@Override
	public void start(Stage newStage) {
		primaryStage = newStage;

		// BorderPane fuer Start Menue
		BorderPane menu = new BorderPane();
		menu.setId("menu");

		// Start Scene fuer Start Menue
		Scene start = new Scene(menu, 800, 650);
		start.getStylesheets().add(getClass().getResource("application.css").toExternalForm()); 

		// Header Bereich im BorderPane
		VBox header = new VBox(5);
		header.getStyleClass().add("header");
		header.setMinSize(800, 200);
		header.setAlignment(Pos.CENTER);
		Text head1 = new Text("Java Projekt");
		head1.getStyleClass().add("head1");
		Text head2 = new Text("Quartett");
		head2.getStyleClass().add("head2");
		header.getChildren().addAll(head1, head2);

		menu.setTop(header);
		// Center Bereich im BorderPane
		VBox center = new VBox(30);
		center.getStyleClass().add("center");
		center.setMaxSize(100, 400);
		center.setAlignment(Pos.BOTTOM_CENTER);

		// Textfeld um Spielernamen festzulegen
		Text head3 = new Text("Gib deinen Namen ein:");
		head3.getStyleClass().add("head3");
		TextField name = new TextField();
		Main.name = name;
		Main.name.setId("textField");
		center.getChildren().addAll(head3, name);
		name.setCursor(Cursor.TEXT);
		menu.setCenter(center);

		// Bottom Bereich im BorderPane
		VBox bottom = new VBox(30);
		bottom.getStyleClass().add("bottom");
		bottom.setAlignment(Pos.CENTER);
		bottom.setMinSize(800, 200);
		bottom.setId("bottom");

		// Start Button
		Button buttonStart = new Button();
		buttonStart.setText(" Start ");
		buttonStart.setCursor(Cursor.HAND);
		menu.setBottom(bottom);
		buttonStart.setId("buttonStart");
		buttonStart.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Ingame newGame = new Ingame();
				newGame.startNewGame(primaryStage);
			}
		});

		// Exit Button
		Button buttonExit = new Button();
		buttonExit.setText("Beenden");
		buttonExit.setCursor(Cursor.HAND);
		buttonExit.setId("buttonExit");
		buttonExit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.exit();
			}
		});
		bottom.getChildren().addAll(buttonStart, buttonExit);
		primaryStage.setTitle("Quartett");
		primaryStage.setScene(start);
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	// Spielernamen Methode
	public TextField getInputText() {
		return Main.name;
	}
}
