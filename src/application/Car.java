package application;

public class Car {
	private int ID; 		// Int ID
	private String name; 	// string Name
	private double price; 	// double Preis
	private int capa; 		// int Hubraum
	private int power; 		// int Leistung
	private double acc; 	// double Beschleunigung
	private int vmax; 		// int Hoechstgeschwindigkeit
	private double cons; 	// double Verbrauch
	private String v1Unit; 	// String Masseinheit von Wert 1
	private String v2Unit; 	// String Masseinheit von Wert 2
	private String v3Unit; 	// String Masseinheit von Wert 3
	private String v4Unit; 	// String Masseinheit von Wert 4
	private String v5Unit; 	// String Masseinheit von Wert 5
	private String v6Unit; 	// String Masseinheit von Wert 6
	private boolean v1LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private boolean v2LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private boolean v3LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private boolean v4LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private boolean v5LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private boolean v6LowToWin; // boolean Soll im Vergleich der kleinere Wert gewinnen?
	private int v1Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten
	private int v2Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten
	private int v3Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten
	private int v4Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten
	private int v5Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten
	private int v6Rank; // int Rang des Wertes dieser Karte mit den entsprechenden Werten der anderen Karten

	// ID - Getter
	public int getID() {
		return ID;
	}

	// ID - Setter
	public void setID(int ID) {
		this.ID = ID;
	}

	// Name - Getter
	public String getName() {
		return name;
	}

	// Name - Setter
	public void setName(String name) {
		this.name = name;
	}

	// Preis - Getter
	public double getPrice() {
		return price;
	}

	// Preis - Setter
	public void setPrice(double price) {
		this.price = price;
	}

	// Hubraum - Getter
	public int getCapa() {
		return capa;
	}

	// Hubraum - Setter
	public void setCapa(int capa) {
		this.capa = capa;
	}

	// Leistung - Getter
	public int getPower() {
		return power;
	}

	// Leistung - Setter
	public void setPower(int power) {
		this.power = power;
	}

	// Beschleunigung - Getter
	public double getAcc() {
		return acc;
	}

	// Beschleunigung - Setter
	public void setAcc(double acc) {
		this.acc = acc;
	}

	// Hoechstgeschwindigkeit - Getter
	public int getVmax() {
		return vmax;
	}

	// Hoechstgeschwindigkeit - Setter
	public void setVmax(int vmax) {
		this.vmax = vmax;
	}

	// Verbrauch - Getter
	public double getCons() {
		return cons;
	}

	// Verbrauch - Setter
	public void setCons(double cons) {
		this.cons = cons;
	}

	// Masseinheit Wert 1 - Getter
	public String getV1Unit() {
		return v1Unit;
	}

	// Masseinheit Wert 1 - Setter
	public void setV1Unit(String v1Unit) {
		this.v1Unit = v1Unit;
	}

	// Masseinheit Wert 2 - Getter
	public String getV2Unit() {
		return v2Unit;
	}

	// Masseinheit Wert 2 - Setter
	public void setV2Unit(String v2Unit) {
		this.v2Unit = v2Unit;
	}

	// Masseinheit Wert 3 - Getter
	public String getV3Unit() {
		return v3Unit;
	}

	// Masseinheit Wert 3 - Setter
	public void setV3Unit(String v3Unit) {
		this.v3Unit = v3Unit;
	}

	// Masseinheit Wert 4 - Getter
	public String getV4Unit() {
		return v4Unit;
	}

	// Masseinheit Wert 4 - Setter
	public void setV4Unit(String v4Unit) {
		this.v4Unit = v4Unit;
	}

	// Masseinheit Wert 5 - Getter
	public String getV5Unit() {
		return v5Unit;
	}

	// Masseinheit Wert 5 - Setter
	public void setV5Unit(String v5Unit) {
		this.v5Unit = v5Unit;
	}

	// Masseinheit Wert 6 - Getter
	public String getV6Unit() {
		return v6Unit;
	}

	// Masseinheit Wert 6 - Setter
	public void setV6Unit(String v6Unit) {
		this.v6Unit = v6Unit;
	}

	// Wert 1 (der niedrigere Wert gewinnt) - Getter
	public boolean getV1LowToWin() {
		return v1LowToWin;
	}

	// Wert 1 (der niedrigere Wert gewinnt) - Setter
	public void setV1LowToWin(boolean v1LowToWin) {
		this.v1LowToWin = v1LowToWin;
	}

	// Wert 2 (der niedrigere Wert gewinnt) - Getter
	public boolean getV2LowToWin() {
		return v2LowToWin;
	}

	// Wert2 (der niedrigere Wert gewinnt) - Setter
	public void setV2LowToWin(boolean v2LowToWin) {
		this.v2LowToWin = v2LowToWin;
	}

	// Wert 3 (der niedrigere Wert gewinnt) - Getter
	public boolean getV3LowToWin() {
		return v3LowToWin;
	}

	// Wert 3 (der niedrigere Wert gewinnt) - Setter
	public void setV3LowToWin(boolean v3LowToWin) {
		this.v3LowToWin = v3LowToWin;
	}

	// Wert 4 (der niedrigere Wert gewinnt) - Getter
	public boolean getV4LowToWin() {
		return v4LowToWin;
	}

	// Wert 4 (der niedrigere Wert gewinnt) - Setter
	public void setV4LowToWin(boolean v4LowToWin) {
		this.v4LowToWin = v4LowToWin;
	}

	// Wert 5 (der niedrigere Wert gewinnt) - Getter
	public boolean getV5LowToWin() {
		return v5LowToWin;
	}

	// Wert 5 (der niedrigere Wert gewinnt) - Setter
	public void setV5LowToWin(boolean v5LowToWin) {
		this.v5LowToWin = v5LowToWin;
	}

	// Wert 6 (der niedrigere Wert gewinnt) - Getter
	public boolean getV6LowToWin() {
		return v6LowToWin;
	}

	// Wert 6 (der niedrigere Wert gewinnt) - Setter
	public void setV6LowToWin(boolean v6LowToWin) {
		this.v6LowToWin = v6LowToWin;
	}

	// Wert1 Rang - Getter
	public int getV1Rank() {
		return v1Rank;
	}

	// Wert1 Rang - Setter
	public void setV1Rank(int v1Rank) {
		this.v1Rank = v1Rank;
	}

	// Wert2 Rang - Getter
	public int getV2Rank() {
		return v2Rank;
	}

	// Wert2 Rang - Setter
	public void setV2Rank(int v2Rank) {
		this.v2Rank = v2Rank;
	}

	// Wert3 Rang - Getter
	public int getV3Rank() {
		return v3Rank;
	}

	// Wert3 Rang - Setter
	public void setV3Rank(int v3Rank) {
		this.v3Rank = v3Rank;
	}

	// Wert4 Rang - Getter
	public int getV4Rank() {
		return v4Rank;
	}

	// Wert4 Rang - Setter
	public void setV4Rank(int v4Rank) {
		this.v4Rank = v4Rank;
	}

	// Wert5 Rang - Getter
	public int getV5Rank() {
		return v5Rank;
	}

	// Wert5 Rang - Setter
	public void setV5Rank(int v5Rank) {
		this.v5Rank = v5Rank;
	}

	// Wert6 Rang - Getter
	public int getV6Rank() {
		return v6Rank;
	}

	// Wert6 Rang - Setter
	public void setV6Rank(int v6Rank) {
		this.v6Rank = v6Rank;
	}
}
