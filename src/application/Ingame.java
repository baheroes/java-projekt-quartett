package application;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Ingame extends Main {
	// Werte fuer Switch Case festlegen
	private static enum ValueType {
		PREIS, HUBRAUM, LEISTUNG, BESCHLEUNIGUNG, GESCHWINDIGKEIT, VERBRAUCH, NONE;
	}

	// BorderPane fuer Ingame + ArrayList
	BorderPane root = new BorderPane();
	private ArrayList<Car> playerOneStack = new ArrayList<Car>();
	private ArrayList<Car> playerTwoStack = new ArrayList<Car>();

	// Button/Label fuer Spieler Karte anlegen
	Button value1 = new Button("value1");
	Button value2 = new Button("value2");
	Button value3 = new Button("value3");
	Button value4 = new Button("value4");
	Button value5 = new Button("value5");
	Button value6 = new Button("value6");
	Label value0 = new Label("Car Name");
	Button selectedButton;
	protected ValueType selectedValue = null;

	// Label fuer Computer Karte anlegen
	Label cValue0 = new Label("Car Name");
	Label cValue1 = new Label("value1");
	Label cValue2 = new Label("value2");
	Label cValue3 = new Label("value3");
	Label cValue4 = new Label("value4");
	Label cValue5 = new Label("value5");
	Label cValue6 = new Label("value6");

	// ImageView fuer Spieler/Computer Karte erstellen
	ImageView computerCarImgView = new ImageView();
	ImageView playerCarImgView = new ImageView();

	// Label der Kartenanzahl anlegen
	Label cNumber = new Label("");
	Label pNumber = new Label("");
	int score = 0;

	// Karten Rueckseite
	VBox cCard = new VBox(0);
	ImageView backside = new ImageView();

	// "Bestaetigen" Button anlegen
	Button buttonOkay = new Button("Bestätigen");

	// Farbe als String festlegen
	String green = "#99FF33";
	String red = "#FF3535";

	// Status Boxen anlegen
	VBox playerScorePanel = new VBox(10);
	VBox computerScorePanel = new VBox(10);
	Label turn = new Label("Du bist dran");

	// Bottom Area anlegen
	HBox bottomArea = new HBox(35);
	VBox gfinish = new VBox(0);
	VBox cfinish = new VBox(0);
	private boolean playersTurn = false;

	public void startNewGame(Stage primaryStage) {
		root.setId("basePane");

		// Szene fuer Ingame festlegen
		Scene ingame = new Scene(root, 800, 650);
		ingame.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		// Headline Ingame "Auto Quartett"
		HBox headerIngame = new HBox(103);
		headerIngame.setMinSize(800, 50);
		headerIngame.setId("headerIngame");
		final Label headline = new Label("Auto Quartett");
		headline.setId("headline");
		// zurueck zum "Menue" Button
		VBox buttonQuitArea = new VBox(30);
		buttonQuitArea.setAlignment(Pos.CENTER_LEFT);
		Button buttonQuit = new Button();
		buttonQuit.setId("buttonQuit");
		buttonQuit.setCursor(Cursor.HAND);
		buttonQuit.setPadding(new Insets(10, 10, 10, 10));
		buttonQuit.setText("Menü");
		buttonQuitArea.getChildren().addAll(buttonQuit);
		// zurueck zum Menue wechseln
		buttonQuit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Main main = new Main();
				main.start(primaryStage);
			}
		});
		// "Spiel Neustarten" Button
		VBox buttonNewArea = new VBox(30);
		buttonNewArea.setAlignment(Pos.CENTER_LEFT);
		Button buttonNew = new Button();
		buttonNew.setId("buttonQuit");
		buttonNew.setCursor(Cursor.HAND);
		buttonNew.setPadding(new Insets(10, 10, 10, 10));
		buttonNew.setText("Neustart");
		buttonNewArea.getChildren().addAll(buttonNew);
		// Spiel neustarten
		buttonNew.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Ingame newGame = new Ingame();
				newGame.startNewGame(primaryStage);
			}
		});
		// Headline, Menue und Neustart dem Header hinzufuegen
		headerIngame.getChildren().addAll(buttonQuitArea, headline, buttonNewArea);
		root.setTop(headerIngame);

		// ######################## Spieler Karte anlegen
		// ########################
		VBox pCard = new VBox(0);
		pCard.setId("pCard");
		pCard.setAlignment(Pos.BOTTOM_CENTER);
		pCard.setMaxSize(310, 460);
		pCard.setMinSize(310, 460);
		Image playerExampleImage = getImage("1.jpg");
		playerCarImgView.setImage(playerExampleImage);
		playerCarImgView.setFitWidth(270);
		playerCarImgView.setPreserveRatio(true);
		value0.getStyleClass().add("value0");
		value1.setMaxWidth(Double.MAX_VALUE);
		value1.setAlignment(Pos.CENTER_LEFT);
		value1.setCursor(Cursor.HAND);
		value1.getStyleClass().add("value1");
		value2.setMaxWidth(Double.MAX_VALUE);
		value2.setAlignment(Pos.CENTER_LEFT);
		value2.setCursor(Cursor.HAND);
		value2.getStyleClass().add("value1");
		value3.setMaxWidth(Double.MAX_VALUE);
		value3.setAlignment(Pos.CENTER_LEFT);
		value3.setCursor(Cursor.HAND);
		value3.getStyleClass().add("value1");
		value4.setMaxWidth(Double.MAX_VALUE);
		value4.setAlignment(Pos.CENTER_LEFT);
		value4.setCursor(Cursor.HAND);
		value4.getStyleClass().add("value1");
		value5.setMaxWidth(Double.MAX_VALUE);
		value5.setAlignment(Pos.CENTER_LEFT);
		value5.setCursor(Cursor.HAND);
		value5.getStyleClass().add("value1");
		value6.setMaxWidth(Double.MAX_VALUE);
		value6.setAlignment(Pos.CENTER_LEFT);
		value6.setCursor(Cursor.HAND);
		value6.getStyleClass().add("value1");
		// Button faerbt sich Grau, beim hovern
		for (Button b : Arrays.asList(value1, value2, value3, value4, value5, value6)) {
			b.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (playersTurn) {
						b.setStyle("-fx-background-color: #c1c8cb;");
					}
				}
			});
			// Button faerbt sich Grau, wenn Value geklickt wird
			b.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (playersTurn) {
						setAllButtonsWhite();
						b.setStyle("-fx-background-color: #c1c8cb;");
					}
				}
			});
			// Button faerbt sich wieder Weiss, wenn nicht geklickt
			b.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (selectedButton == b && playersTurn) {
						b.setStyle("-fx-background-color: #c1c8cb;");
					} else
						b.setStyle("-fx-background-color: white;");
				}
			});
		}
		// Bild und Values der Spielerkarte hinzufuegen
		pCard.getChildren().addAll(playerCarImgView, value0, value1, value2, value3, value4, value5, value6);
		StackPane centerLeft = new StackPane();
		centerLeft.setId("centerLeft");
		centerLeft.getChildren().addAll(pCard);
		root.setLeft(centerLeft);

		// ######################## Computer Karte anlegen
		// ########################
		cCard.setAlignment(Pos.BOTTOM_CENTER);
		cCard.setId("cCard");
		cCard.setMaxSize(310, 460);
		cCard.setMinSize(310, 460);

		// Rueckseite der Computer Karte
		Image back = getImage("backside.png");
		backside.setImage(back);
		backside.setFitHeight(460);
		backside.setFitWidth(325);
		backside.setPreserveRatio(true);
		backside.setId("backside");
		// Bild auf Spielerkarte des Computers
		Image computerExampleImage = getImage("2.jpg");
		computerCarImgView.setImage(computerExampleImage);
		computerCarImgView.setFitWidth(270);
		computerCarImgView.setPreserveRatio(true);
		cValue0.getStyleClass().add("cValue0");
		cValue1.setMaxWidth(Double.MAX_VALUE);
		cValue1.setAlignment(Pos.CENTER_LEFT);
		cValue1.getStyleClass().add("cValue1");
		cValue2.setMaxWidth(Double.MAX_VALUE);
		cValue2.setAlignment(Pos.CENTER_LEFT);
		cValue2.getStyleClass().add("cValue1");
		cValue3.setMaxWidth(Double.MAX_VALUE);
		cValue3.setAlignment(Pos.CENTER_LEFT);
		cValue3.getStyleClass().add("cValue1");
		cValue4.setMaxWidth(Double.MAX_VALUE);
		cValue4.setAlignment(Pos.CENTER_LEFT);
		cValue4.getStyleClass().add("cValue1");
		cValue5.setMaxWidth(Double.MAX_VALUE);
		cValue5.setAlignment(Pos.CENTER_LEFT);
		cValue5.getStyleClass().add("cValue1");
		cValue6.setMaxWidth(Double.MAX_VALUE);
		cValue6.setAlignment(Pos.CENTER_LEFT);
		cValue6.getStyleClass().add("cValue1");
		// Bild und Values Computer Karte hinzufuegen
		cCard.getChildren().addAll(computerCarImgView, cValue0, cValue1, cValue2, cValue3, cValue4, cValue5, cValue6);
		StackPane centerRight = new StackPane();
		centerRight.setId("centerRight");
		centerRight.getChildren().addAll(cCard, backside);
		root.setRight(centerRight);

		// ######################## Bottom Bereich des BorderPane
		// ########################
		StackPane bottom = new StackPane();
		bottomArea.setMinSize(800, 30);
		turn.setId("turn");

		// Score Panel Spieler
		playerScorePanel.setId("playerScorePanel");
		HBox player = new HBox();
		player.setId("player");
		Label name = new Label();
		name.setText(getInputText().getText());
		player.getChildren().addAll(new Label("Spieler: "), name);
		playerScorePanel.getChildren().addAll(player, pNumber, turn);

		// Button Zug bestaetigen
		buttonOkay.setId("buttonOkay");
		buttonOkay.setCursor(Cursor.HAND);
		buttonOkay.setVisible(false);

		// Score Panel Computer
		Label computer = new Label("------- Computer ------");
		computer.setId("player");
		computerScorePanel.getChildren().addAll(computer, cNumber);
		computerScorePanel.setId("computerScorePanel");
		bottomArea.setId("bottomArea");
		bottomArea.getChildren().addAll(playerScorePanel, buttonOkay, computerScorePanel);
		bottom.getChildren().addAll(cfinish, bottomArea);
		root.setBottom(bottomArea);

		primaryStage.setTitle("Quartett");
		primaryStage.setScene(ingame);
		primaryStage.setResizable(false);
		primaryStage.show();

		// ######################## Eigentlicher Spielbeginn
		// ########################

		// CSV-Objekte erstellen
		ArrayList<Car> cardStack = DeckReader.readDeck();

		// Rangliste erstellen gemaess der Methode in Klasse "RankData"
		ArrayList<RankData> rankList = (new RankData()).createRankList();

		// Rang der einzelnen Werte aus der Rangliste in Hauptkartenstapel
		// uebertragen
		for (int i = 0; i < cardStack.size(); i++) {
			int a = rankList.get(i).getVal1Rank();
			cardStack.get(i).setV1Rank(a);
			int b = rankList.get(i).getVal2Rank();
			cardStack.get(i).setV2Rank(b);
			int c = rankList.get(i).getVal3Rank();
			cardStack.get(i).setV3Rank(c);
			int d = rankList.get(i).getVal4Rank();
			cardStack.get(i).setV4Rank(d);
			int e = rankList.get(i).getVal5Rank();
			cardStack.get(i).setV5Rank(e);
			int f = rankList.get(i).getVal6Rank();
			cardStack.get(i).setV6Rank(f);
		}

		// erzeuge Array mit der Anzahl der Spielkarten im Set
		int mainStack = 32;
		ArrayList<Integer> randomChoice = new ArrayList<Integer>();
		for (int i = 0; i < mainStack; i++) {
			randomChoice.add(i);
		}

		// Karten "mischen"
		Collections.shuffle(randomChoice);

		// Karten verteilen fuer Spieler 1
		for (int i = 0; i < 8; i++) {
			Car karre = new Car();
			karre = cardStack.get(randomChoice.get(i));
			playerOneStack.add(karre);
		}

		// Stapel 1 ausgeben
		System.out.println("Spieler 1 hat folgende Karten auf der hand:");
		for (int i = 0; i < 8; i++) {
			Car karre = new Car();
			karre = playerOneStack.get(i);
			System.out.println(karre.getName());
		}

		// Karten verteilen fuer Spieler 2
		for (int i = 8; i < 16; i++) {
			Car karre = new Car();
			karre = cardStack.get(randomChoice.get(i));
			playerTwoStack.add(karre);
		}

		// Stapel 2 ausgeben
		System.out.println("Spieler 2 hat folgende Karten auf der hand:");
		for (int i = 0; i < 8; i++) {

			Car karre = new Car();
			karre = playerTwoStack.get(i);
			System.out.println(karre.getName());
		}

		// Eventhandler fuer Spielerkarte
		value1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.PREIS;
					selectedButton = value1;
				}
			}
		});
		// Eventhandler fuer Spielerkarte
		value2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.HUBRAUM;
					selectedButton = value2;
				}
			}
		});
		// Eventhandler fuer Spielerkarte
		value3.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.LEISTUNG;
					selectedButton = value3;
				}
			}
		});
		// Eventhandler fuer Spielerkarte
		value4.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.BESCHLEUNIGUNG;
					selectedButton = value4;
				}
			}
		});
		// Eventhandler fuer Spielerkarte
		value5.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.GESCHWINDIGKEIT;
					selectedButton = value5;
				}
			}
		});
		// Eventhandler fuer Spielerkarte
		value6.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					buttonOkay.setVisible(true);
					selectedValue = ValueType.VERBRAUCH;
					selectedButton = value6;
				}
			}
		});

		// Bestaetigen der Auswahl
		buttonOkay.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (playersTurn) {
					playersTurn = false;
					buttonOkay.setVisible(false);
					playerMove(selectedValue, playerOneStack, playerTwoStack);
				}
			}
		});

		setValueToCard();

		playersTurn = true;
	}

	// Fehlermeldung falls Bild nicht gefunden wird
	private Image getImage(String name) {
		URL url = getClass().getResource("/application/images/" + name);
		try {
			return new Image(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	// Button Farbe zuruecksetzen
	protected void setAllButtonsWhite() {
		value1.setStyle("-fx-background-color: white;");
		value2.setStyle("-fx-background-color: white;");
		value3.setStyle("-fx-background-color: white;");
		value4.setStyle("-fx-background-color: white;");
		value5.setStyle("-fx-background-color: white;");
		value6.setStyle("-fx-background-color: white;");
	}

	// Methode Values der Karte hinzufuegen
	public void setValueToCard() {

		java.text.NumberFormat priceForm = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.GERMANY);

		// Spieler Karte
		String name = playerOneStack.get(0).getName();
		double price = playerOneStack.get(0).getPrice();
		int capa = playerOneStack.get(0).getCapa();
		String v2Unit = playerOneStack.get(0).getV2Unit();
		int power = playerOneStack.get(0).getPower();
		String v3Unit = playerOneStack.get(0).getV3Unit();
		double acc = playerOneStack.get(0).getAcc();
		String v4Unit = playerOneStack.get(0).getV4Unit();
		int vmax = playerOneStack.get(0).getVmax();
		String v5Unit = playerOneStack.get(0).getV5Unit();
		double cons = playerOneStack.get(0).getCons();
		String v6Unit = playerOneStack.get(0).getV6Unit();
		String imgId = Integer.toString(playerOneStack.get(0).getID());
		Image playerCarImg = getImage(imgId + ".jpg");
		playerCarImgView.setImage(playerCarImg);
		value0.setText(name);
		value1.setText("Preis:\t\t" + String.valueOf(priceForm.format(price)) + " ↑");
		value2.setText("Hubraum:\t" + String.valueOf(capa) + " " + v2Unit + " ↑");
		value3.setText("Leistung:\t" + String.valueOf(power) + " " + v3Unit + " ↑");
		value4.setText("Beschleunigung:\t" + String.valueOf(acc) + " " + v4Unit + " ↓");
		value5.setText("Höchstgeschw.:\t" + String.valueOf(vmax) + " " + v5Unit + " ↑");
		value6.setText("Verbrauch:\t" + String.valueOf(cons) + " " + v6Unit + " ↓");
		String cardsInPlayerStack = String.valueOf(playerOneStack.size());
		pNumber.setText("Karten auf der Hand: " + cardsInPlayerStack);
		// Computer Karte
		String Cname = playerTwoStack.get(0).getName();
		double Cprice = playerTwoStack.get(0).getPrice();
		int Ccapa = playerTwoStack.get(0).getCapa();
		String Cv2Unit = playerTwoStack.get(0).getV2Unit();
		int Cpower = playerTwoStack.get(0).getPower();
		String Cv3Unit = playerTwoStack.get(0).getV3Unit();
		double Cacc = playerTwoStack.get(0).getAcc();
		String Cv4Unit = playerTwoStack.get(0).getV4Unit();
		int Cvmax = playerTwoStack.get(0).getVmax();
		String Cv5Unit = playerTwoStack.get(0).getV5Unit();
		double Ccons = playerTwoStack.get(0).getCons();
		String Cv6Unit = playerTwoStack.get(0).getV6Unit();
		String computerImgId = Integer.toString(playerTwoStack.get(0).getID());
		Image computerCarImg = getImage(computerImgId + ".jpg");
		computerCarImgView.setImage(computerCarImg);
		cValue0.setText(Cname);
		cValue1.setText("Preis:\t\t" + String.valueOf(priceForm.format(Cprice)));
		cValue2.setText("Hubraum:\t" + String.valueOf(Ccapa) + " " + Cv2Unit);
		cValue3.setText("Leistung:\t" + String.valueOf(Cpower) + " " + Cv3Unit);
		cValue4.setText("Beschleunigung:\t" + String.valueOf(Cacc) + " " + Cv4Unit);
		cValue5.setText("Höchstgeschw.:\t" + String.valueOf(Cvmax) + " " + Cv5Unit);
		cValue6.setText("Verbrauch:\t" + String.valueOf(Ccons) + " " + Cv6Unit);
		String cardsInComputerStack = String.valueOf(playerTwoStack.size());
		cNumber.setText("Karten auf der Hand: " + cardsInComputerStack);
	}

	// Spielzug Spieler
	public void playerMove(ValueType v, List<Car> pList, List<Car> cList) {
		System.out.println("player move");
		Animation.flipBackToFront(cCard, backside, new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				switch (v) {
				case PREIS:
					double a = ((Car) pList.get(0)).getPrice();
					double b = ((Car) cList.get(0)).getPrice();
					boolean priceLowToWin = ((Car) pList.get(0)).getV1LowToWin();
					if (priceLowToWin == false) {
						if (a > b) {
							score = 1;
							value1.setStyle("-fx-background-color: " + green);
							cValue1.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value1.setStyle("-fx-background-color: " + red);
							cValue1.setStyle("-fx-background-color: " + green);
						}
					} else {
						if (a > b) {
							score = 0;
							value1.setStyle("-fx-background-color: " + red);
							cValue1.setStyle("-fx-background-color: " + green);
						} else {
							score = 1;
							value1.setStyle("-fx-background-color: " + green);
							cValue1.setStyle("-fx-background-color: " + red);
						}
					}
					break;

				case HUBRAUM:
					int c = ((Car) pList.get(0)).getCapa();
					int d = ((Car) cList.get(0)).getCapa();
					boolean capaLowToWin = ((Car) pList.get(0)).getV2LowToWin();
					if (capaLowToWin == false) {
						if (c > d) {
							score = 1;
							value2.setStyle("-fx-background-color: " + green);
							cValue2.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value2.setStyle("-fx-background-color: " + red);
							cValue2.setStyle("-fx-background-color: " + green);
						}
					} else {
						if (c > d) {
							score = 0;
							value2.setStyle("-fx-background-color: " + red);
							cValue2.setStyle("-fx-background-color: " + green);
						} else {
							score = 1;
							value2.setStyle("-fx-background-color: " + green);
							cValue2.setStyle("-fx-background-color: " + red);
						}
					}
					break;

				case LEISTUNG:
					int e = ((Car) pList.get(0)).getPower();
					int f = ((Car) cList.get(0)).getPower();
					boolean PowerLowToWin = ((Car) pList.get(0)).getV3LowToWin();
					if (PowerLowToWin == false) {
						if (e > f) {
							score = 1;
							value3.setStyle("-fx-background-color: " + green);
							cValue3.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value3.setStyle("-fx-background-color: " + red);
							cValue3.setStyle("-fx-background-color: " + green);
						}
					} else {
						if (e > f) {
							score = 0;
							value3.setStyle("-fx-background-color: " + red);
							cValue3.setStyle("-fx-background-color: " + green);
						} else {
							score = 1;
							value3.setStyle("-fx-background-color: " + green);
							cValue3.setStyle("-fx-background-color: " + red);
						}
					}
					break;

				case BESCHLEUNIGUNG:
					double g = ((Car) pList.get(0)).getAcc();
					double h = ((Car) cList.get(0)).getAcc();
					boolean AccLowToWin = ((Car) pList.get(0)).getV4LowToWin();
					if (AccLowToWin == false) {
						if (g > h) {
							score = 1;
							value4.setStyle("-fx-background-color: " + green);
							cValue4.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value4.setStyle("-fx-background-color: " + red);
							cValue4.setStyle("-fx-background-color: " + green);
						}
					} else {
						if (g > h) {
							score = 0;
							value4.setStyle("-fx-background-color: " + red);
							cValue4.setStyle("-fx-background-color: " + green);
						} else {
							score = 1;
							value4.setStyle("-fx-background-color: " + green);
							cValue4.setStyle("-fx-background-color: " + red);
						}
					}
					break;

				case GESCHWINDIGKEIT:
					int i = ((Car) pList.get(0)).getVmax();
					int j = ((Car) cList.get(0)).getVmax();
					boolean VmaxLowToWin = ((Car) pList.get(0)).getV5LowToWin();
					if (VmaxLowToWin == false) {
						if (i > j) {
							score = 1;
							value5.setStyle("-fx-background-color: " + green);
							cValue5.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value5.setStyle("-fx-background-color: " + red);
							cValue5.setStyle("-fx-background-color: " + green);
						}
					} else {
						if (i > j) {
							score = 0;
							value5.setStyle("-fx-background-color: " + red);
							cValue5.setStyle("-fx-background-color: " + green);
						} else {
							score = 1;
							value5.setStyle("-fx-background-color: " + green);
							cValue5.setStyle("-fx-background-color: " + red);
						}
					}
					break;

				case VERBRAUCH:
					double k = ((Car) pList.get(0)).getCons();
					double l = ((Car) cList.get(0)).getCons();
					boolean ConsLowToWin = ((Car) pList.get(0)).getV6LowToWin();
					if (ConsLowToWin == false) {
						if (k > l) {
							score = 1;
							value6.setStyle("-fx-background-color: " + green);
							cValue6.setStyle("-fx-background-color: " + red);
						} else {
							score = 0;
							value6.setStyle("-fx-background-color: " + red);
							cValue6.setStyle("-fx-background-color: " + green);
							System.out.println("sollte false sein: " + ConsLowToWin);
						}
					} else {
						if (k > l) {
							score = 0;
							value6.setStyle("-fx-background-color: " + red);
							cValue6.setStyle("-fx-background-color: " + green);
							System.out.println("sollte true sein: " + ConsLowToWin);
						} else {
							score = 1;
							value6.setStyle("-fx-background-color: " + green);
							cValue6.setStyle("-fx-background-color: " + red);
							System.out.println("sollte true sein: " + ConsLowToWin);
						}
					}
					break;
				default:
					score = 1;
					break;
				}
				evaluation(score, playerOneStack, playerTwoStack);
			}
		});
	}

	// Spielzug Computer
	public void computerMove(List<Car> pList, List<Car> cList) {
		System.out.println("computer move");
		int a = (cList.get(0)).getV1Rank();
		int b = (cList.get(0)).getV2Rank();
		int c = (cList.get(0)).getV3Rank();
		int d = (cList.get(0)).getV4Rank();
		int e = (cList.get(0)).getV5Rank();
		int f = (cList.get(0)).getV6Rank();
		int[] indexWatch;
		indexWatch = new int[] { a, b, c, d, e, f };
		System.out.format("Ränge: %d, %d, %d, %d, %d, %d", a, b, c, d, e, f);
		System.out.println(cList.get(0).getName());

		// finde den Wert mit dem hoechsten Rang
		int maxIndex = 0;
		for (int i = 1; i < indexWatch.length; i++) {
			int newnumber = indexWatch[i];
			if ((newnumber > indexWatch[maxIndex])) {
				maxIndex = i;
			}
		}
		// Entscheide anhand des hoechsten Ranges, welcher Wert fuer den
		// Spielzug ausgewaehlt wird
		switch (maxIndex) {
		case 0:
			double g = ((Car) cList.get(0)).getPrice();
			double h = ((Car) pList.get(0)).getPrice();
			boolean priceLowToWin = ((Car) cList.get(0)).getV1LowToWin();
			System.out.println("der 1. Streich");
			if (priceLowToWin == false) {
				if (g > h) {
					score = 0;
					cValue1.setStyle("-fx-background-color: " + green);
					value1.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value1.setStyle("-fx-background-color: " + green);
					cValue1.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (g > h) {
					score = 1;
					value1.setStyle("-fx-background-color: " + green);
					cValue1.setStyle("-fx-background-color: " + red);
				} else {
					System.out.println("computer rules");
					score = 0;
					value1.setStyle("-fx-background-color: " + red);
					cValue1.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		case 1:
			int i = ((Car) cList.get(0)).getCapa();
			int j = ((Car) pList.get(0)).getCapa();
			boolean capaLowToWin = ((Car) cList.get(0)).getV2LowToWin();
			System.out.println("der 2. Streich");
			if (capaLowToWin == false) {
				if (i > j) {
					score = 0;
					cValue2.setStyle("-fx-background-color: " + green);
					value2.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value2.setStyle("-fx-background-color: " + green);
					cValue2.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (i > j) {
					score = 1;
					value2.setStyle("-fx-background-color: " + green);
					cValue2.setStyle("-fx-background-color: " + red);
				} else {
					score = 0;
					value2.setStyle("-fx-background-color: " + red);
					cValue2.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		case 2:
			int k = ((Car) cList.get(0)).getPower();
			int l = ((Car) pList.get(0)).getPower();
			boolean powerLowToWin = ((Car) cList.get(0)).getV3LowToWin();
			System.out.println("der 3. Streich");
			if (powerLowToWin == false) {
				if (k > l) {
					score = 0;
					cValue3.setStyle("-fx-background-color: " + green);
					value3.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value3.setStyle("-fx-background-color: " + green);
					cValue3.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (k > l) {
					score = 1;
					value3.setStyle("-fx-background-color: " + green);
					cValue3.setStyle("-fx-background-color: " + red);
				} else {
					score = 0;
					value3.setStyle("-fx-background-color: " + red);
					cValue3.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		case 3:
			double m = ((Car) cList.get(0)).getAcc();
			double n = ((Car) pList.get(0)).getAcc();
			boolean accLowToWin = ((Car) cList.get(0)).getV4LowToWin();
			if (accLowToWin == false) {
				if (m > n) {
					score = 0;
					cValue4.setStyle("-fx-background-color: " + green);
					value4.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value4.setStyle("-fx-background-color: " + green);
					cValue4.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (m > n) {
					score = 1;
					value4.setStyle("-fx-background-color: " + green);
					cValue4.setStyle("-fx-background-color: " + red);
				} else {
					System.out.println("computer rules");
					score = 0;
					value4.setStyle("-fx-background-color: " + red);
					cValue4.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		case 4:
			int o = ((Car) cList.get(0)).getVmax();
			int p = ((Car) pList.get(0)).getVmax();
			boolean vmaxLowToWin = ((Car) cList.get(0)).getV5LowToWin();
			if (vmaxLowToWin == false) {
				if (o > p) {
					score = 0;
					cValue5.setStyle("-fx-background-color: " + green);
					value5.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value5.setStyle("-fx-background-color: " + green);
					cValue5.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (o > p) {
					score = 1;
					value5.setStyle("-fx-background-color: " + green);
					cValue5.setStyle("-fx-background-color: " + red);
				} else {
					score = 0;
					value1.setStyle("-fx-background-color: " + red);
					cValue1.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		case 5:
			double q = ((Car) cList.get(0)).getCons();
			double r = ((Car) pList.get(0)).getCons();
			boolean consLowToWin = ((Car) cList.get(0)).getV6LowToWin();
			if (consLowToWin == false) {
				if (q > r) {
					score = 0;
					cValue6.setStyle("-fx-background-color: " + green);
					value6.setStyle("-fx-background-color: " + red);
				} else {
					score = 1;
					value6.setStyle("-fx-background-color: " + green);
					cValue6.setStyle("-fx-background-color: " + red);
				}
			} else {
				if (q > r) {
					score = 1;
					value6.setStyle("-fx-background-color: " + green);
					cValue6.setStyle("-fx-background-color: " + red);
				} else {
					score = 0;
					value6.setStyle("-fx-background-color: " + red);
					cValue6.setStyle("-fx-background-color: " + green);
				}
			}
			break;
		default:
			score = 1;
			break;
		}
		evaluation(score, pList, cList);
	}

	// Bottom Bereich, wenn Spiel zu Ende ist
	public void finish(int x) {
		if (x == 1) {
			// wenn Spieler gewonnen hat
			gfinish.setId("finish");
			gfinish.setMinSize(800, 50);
			gfinish.setAlignment(Pos.CENTER);
			Label gwinnerText = new Label("Glückwunsch, Du hast gewonnen!");
			gwinnerText.setId("winnerText");
			Button buttonStart = new Button();
			buttonStart.setText("Neustart");
			buttonStart.setCursor(Cursor.HAND);
			buttonStart.setId("buttonStart");
			// Szene neuladen
			buttonStart.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Ingame newGame = new Ingame();
					newGame.startNewGame(getStage());
				}
			});
			gfinish.getChildren().addAll(gwinnerText, buttonStart);
			root.setBottom(gfinish);

		} else if (x == 0) {
			// wenn Computer gewonnen hat
			cfinish.setId("finish");
			cfinish.setMinSize(800, 50);
			cfinish.setAlignment(Pos.CENTER);
			Label cwinnerText = new Label("Schade, der Computer hat gewonnen!");
			cwinnerText.setId("winnerText");
			Button buttonStart = new Button();
			buttonStart.setText("Neustart");
			buttonStart.setCursor(Cursor.HAND);
			buttonStart.setId("buttonStart");
			// Szene neuladen
			buttonStart.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Ingame newGame = new Ingame();
					newGame.startNewGame(getStage());
				}
			});
			cfinish.getChildren().addAll(cwinnerText, buttonStart);
			root.setBottom(cfinish);
		}

	}

	// Spielzug auswerten
	public void evaluation(int e, List<Car> pList, List<Car> cList) {
		// Spieler Gewinnt:
		if (e == 1) {
			Car transferCar = new Car();
			transferCar = (Car) cList.get(0);
			pList.add(0, transferCar);
			cList.remove(0);
			int playerWin = pList.size();
			if (playerWin == 16) {
				finish(1);
				return;

			}
			Collections.rotate(pList, 1);
			String panelCheck = turn.getText();
			if (panelCheck != "Du bist dran") {
				computerScorePanel.getChildren().remove(turn);
				turn.setText("Du bist dran");
				playerScorePanel.getChildren().add(turn);
			}
		}
		// Computer gewinnt
		else {
			Car transferCar = new Car();
			transferCar = (Car) pList.get(0);
			cList.add(0, transferCar);
			pList.remove(0);
			int computerWin = cList.size();
			if (computerWin == 16) {
				finish(0);
				return;
			}

			Collections.rotate(cList, 1);
			String panelCheck = turn.getText();
			if (panelCheck != "Der Computer ist dran") {
				playerScorePanel.getChildren().remove(turn);
				turn.setText("Der Computer ist dran");
				computerScorePanel.getChildren().add(turn);
			}
		}
		// Button weiss, Computer Karte umdrehen
		EventHandler<ActionEvent> handler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setAllButtonsWhite();
				if (e == 1)
					Animation.flipFrontToBack(cCard, backside, null);

				// neuen Zug vorbereiten
				setValueToCard();
				selectedValue = ValueType.NONE;
				selectedButton = null;

				for (Label l : Arrays.asList(cValue1, cValue2, cValue3, cValue4, cValue5, cValue6)) {
					l.setStyle("-fx-background-color: white");
				}

				// Spielzug des Computers starten
				if (e == 0) {
					computerMove(playerOneStack, playerTwoStack);
				} else {
					playersTurn = true;
				}
			}
		};
		new Timeline(new KeyFrame(Duration.millis(2500), handler)).play();
	}

	// Komplette Liste aller Autos ausgeben
	public void printCarList(List<Car> carListToPrint) {
		for (int i = 0; i < carListToPrint.size(); i++) {
			System.out.println("CARS [" + "ID=" + (carListToPrint.get(i)).getID() + " , Name= "
					+ (carListToPrint.get(i)).getName() + " , Preis=" + (carListToPrint.get(i)).getPrice()
					+ (carListToPrint.get(i)).getV1Unit() + "(Gesamtrang: " + (carListToPrint.get(i)).getV1Rank() + ")"
					+ " , Hubraum=" + (carListToPrint.get(i)).getCapa() + (carListToPrint.get(i)).getV2Unit()
					+ "(Gesamtrang: " + (carListToPrint.get(i)).getV2Rank() + ")" + " , Leistung="
					+ (carListToPrint.get(i)).getPower() + (carListToPrint.get(i)).getV3Unit() + "(Gesamtrang: "
					+ (carListToPrint.get(i)).getV3Rank() + ")" + " , Beschleunigung="
					+ (carListToPrint.get(i)).getAcc() + (carListToPrint.get(i)).getV4Unit() + "(Gesamtrang: "
					+ (carListToPrint.get(i)).getV4Rank() + ")" + " , Geschwindigkeit="
					+ (carListToPrint.get(i)).getVmax() + (carListToPrint.get(i)).getV5Unit() + "(Gesamtrang: "
					+ (carListToPrint.get(i)).getV5Rank() + ")" + " , Verbrauch=" + (carListToPrint.get(i)).getCons()
					+ (carListToPrint.get(i)).getV6Unit() + "(Gesamtrang: " + (carListToPrint.get(i)).getV6Rank() + ")"
					+ "]");
		}
	}
}